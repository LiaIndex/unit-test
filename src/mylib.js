// src/mylib.js
module.exports = {
    sum: (a, b) => a + b,
    random: () => Math.random(),
    arrayGen: () => [1,2,3],
    myfunc1: (a,b) => a - b,
    myfunc2: (a,b) => Math.pow(a,b)
    }
    
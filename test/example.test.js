// example.test.js
const expect = require('chai').expect;
const should = require('chai').should();
const { assert } = require('chai');
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {
    let myvar = undefined;
    let mya = undefined;
    let myb = undefined;
    // runs after all tests in this block
    after(() => {
        console.log("test ended");
        console.log("to say goodby im going to substract 2 - 1"); 
        console.log(mylib.myfunc1(2, 1));
    });
    // unit test for the mylib.sum function. Expect
    it.skip('Should return 2 when using sum function with a=myvar, b=1', () => {
        const result = mylib.sum(1, 1); // 1 + 1
        expect(result).to.equal(2); // result expected to equal 2
    });
    // run tasks before tests in this block
    before(() => {
        myvar = 1; // setup before testing
        mya = 2;
        myb = 3;
        console.log('Before testing im going to do 2^5... it is: ' );
        console.log(mylib.myfunc2(2,5));
    });
    it.skip('Assert foo is not bar', () => {
        assert('foo' !== 'bar');
    })
    it.skip('Myvar should exist', () => {
        should.exist(myvar)
    })
    it('testing myfunc1... Should return -1 when using myfunc1 function with a=2, b=3', () => {
        const result = mylib.myfunc1(mya, myb); // 2 - 3 = -1
        expect(result).to.equal(-1); // result expected to equal 2
    });
    it('testing myfunc2... Should return 8 when using myfunc2 function with a=2, b=3', () => {
        const result = mylib.myfunc2(mya, myb); // 2 ^ 3 = 8
        assert(result == 8); // result expected to equal 2
    });
    // test will randomly fail
     it.skip('Random', () => expect(mylib.random()).to.above(0.5));
});
